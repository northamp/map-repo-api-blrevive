package main

type GameMapStorage interface {
	List() []*GameMapMetadata
	Get(string, string) *GameMapMetadata
	Head(string, string) *GameMapMetadata
	// Update(string, string) *GameMapMetadata
	Create(string, string) *GameMapMetadata
	Delete(string, string) *GameMapMetadata
}

type GameMapStore struct {
}

type GameMapMetadata struct {
	Name    string `json:"name"`
	Author  string `json:"author"`
	Version string `json:"version"`
}

func (mapStore GameMapStore) List() []*GameMapMetadata {
	// List all maps in S3 bucket by looking for each objects with path Author.Name
}

func (mapStore GameMapStore) Head(name string, author string) *GameMapMetadata {
	// Get map metadata from S3 bucket by looking for Author.Name/Author.Name_metadata.json
}

func (mapStore GameMapStore) Get(name string, author string) *GameMapMetadata {
	// Get map from S3 bucket by returning pre-signed URL that allows downloading an object with path Author.Name/Author.Name_map.zip
}

func (mapStore GameMapStore) Create(name string, author string, version string) *GameMapMetadata {
	// Return pre-signed URL that allows creating an object in the S3 bucket with path Author.Name/Author.Name_map.zip
}

func (mapStore GameMapStore) Delete(name string, author string) *GameMapMetadata {
	// Delete all objects in S3 bucket with path Author.Name
}

// Redundant?
// func (mapStore GameMapStore) Update(name string, author string) *GameMapMetadata {
// 	// Return pre-signed URL that allows creating an object in the S3 bucket with path Author.Name/Author.Name_map.zip and change version in Author.Name/metadata.json
// }
