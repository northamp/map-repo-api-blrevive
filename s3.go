package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	s3config "github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

func CreateObjectStorageClient(config *Config) (*s3.Client, error) {
	switch config.S3.Flavor {
	case "R2":
		return CreatesR2Client(config)
	case "AWS":
		return nil, fmt.Errorf("unsupported flavor: %s", config.S3.Flavor)
	default:
		return nil, fmt.Errorf("unknown flavor: %s", config.S3.Flavor)
	}
}

func CreatesR2Client(config *Config) (client *s3.Client, err error) {
	r2Resolver := aws.EndpointResolverWithOptionsFunc(func(service, region string, options ...interface{}) (aws.Endpoint, error) {
		return aws.Endpoint{
			URL: fmt.Sprintf("https://%s.r2.cloudflarestorage.com", config.S3.AccountId),
		}, nil
	})

	cfg, err := s3config.LoadDefaultConfig(context.TODO(),
		s3config.WithEndpointResolverWithOptions(r2Resolver),
		s3config.WithCredentialsProvider(credentials.NewStaticCredentialsProvider(config.S3.AccessKeyId, config.S3.AccessKeySecret, "")),
		s3config.WithRegion("auto"),
	)
	if err != nil {
		return nil, err
	}

	return s3.NewFromConfig(cfg), err
}
