package main

import (
	"net/http"

	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/go-chi/chi/v5"
)

type GameMapHandler struct {
	storage  GameMapStorage
	s3client *s3.Client
}

func (handler GameMapHandler) ListGameMaps(writer http.ResponseWriter, request *http.Request) {
	// List all maps
}

func (handler GameMapHandler) HeadGameMap(writer http.ResponseWriter, request *http.Request) {
	name := chi.URLParam(request, "name")
	author := chi.URLParam(request, "author")
	// Get map metadata return in headers
}

func (handler GameMapHandler) GetGameMap(writer http.ResponseWriter, request *http.Request) {
	name := chi.URLParam(request, "name")
	author := chi.URLParam(request, "author")
	// Generate pre-signed URL to download map's zip, return as 302
}

func (handler GameMapHandler) CreateGameMap(writer http.ResponseWriter, request *http.Request) {
	name := chi.URLParam(request, "name")
	author := chi.URLParam(request, "author")
	version := chi.URLParam(request, "version")
	// Create map metadata, return pre-signed URL to upload map
}

// Redundant?
// func (handler GameMapHandler) UpdateGameMap(writer http.ResponseWriter, request *http.Request) {
// 	name := chi.URLParam(request, "name")
// 	author := chi.URLParam(request, "author")
// 	// Return pre-signed URL to upload map
// }

func (handler GameMapHandler) DeleteGameMap(writer http.ResponseWriter, request *http.Request) {
	name := chi.URLParam(request, "name")
	author := chi.URLParam(request, "author")
	// Delete entire prefix
}
