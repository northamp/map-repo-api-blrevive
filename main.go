package main

import (
	"net/http"
	"os"
	"strings"
	"time"

	logger "github.com/chi-middleware/logrus-logger"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"github.com/sirupsen/logrus"

	"github.com/spf13/viper"
)

var Version = "dev" // Set by build flag: go build -ldflags="-X 'main.Version=1.0.0'"

var log = logrus.New()

func init() {
	log.SetOutput(os.Stdout)
}

type Config struct {
	Debug     string `mapstructure:"Debug"`
	LogLevel  string `mapstructure:"LogLevel"`
	WebServer struct {
		Address string `mapstructure:"Address"`
	}
	S3 struct {
		Flavor          string `mapstructure:"Flavor"`
		BucketName      string `mapstructure:"BucketName"`
		AccountId       string `mapstructure:"AccountId"`
		AccessKeyId     string `mapstructure:"AccessKeyId"`
		AccessKeySecret string `mapstructure:"AccessKeySecret"`
	} `mapstructure:"S3"`
}

func DetermineLogLevel(LogLevel string) {
	switch logLevel := LogLevel; strings.ToLower(logLevel) {
	case "trace":
		log.SetLevel(logrus.TraceLevel)
	case "debug":
		log.SetLevel(logrus.DebugLevel)
	case "info":
		log.SetLevel(logrus.InfoLevel)
	default:
		log.SetLevel(logrus.InfoLevel)
	}
}

func main() {
	log.Info("API starting up")

	// Look for config.json in /etc/blre-map-api/config.json, C:\ProgramData\blre-map-api\config.json, and the execution dir
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath("/etc/blre-map-api/")
	viper.AddConfigPath("C:\\ProgramData\\blre-map-api\\")
	viper.AddConfigPath(".")

	// Defaults - will be overwritten by JSON or env vars, favoring the former
	viper.SetDefault("Debug", false)
	viper.SetDefault("LogLevel", "info")

	viper.SetDefault("WebServer.Address", ":80")

	// Read from env vars
	viper.SetEnvPrefix("BLREMAPAPI")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	// NOTE: Add -tags=viper_bind_struct to build commands til https://github.com/spf13/viper/issues/1706 is sorted out (https://github.com/spf13/viper/pull/1429)
	viper.AutomaticEnv()

	// Read from JSON
	if err := viper.ReadInConfig(); err != nil {
		log.WithField("error", err).Info("No configuration file found - relying on env vars and defaults")
	} else {
		log.WithField("configFile", viper.ConfigFileUsed()).Info("Found configuration file")
	}

	// Set log level
	DetermineLogLevel(viper.GetString("LogLevel"))

	// Unmarshal config to pass only a conf object down the line instead of an entire viper
	var config Config
	if err := viper.Unmarshal(&config); err != nil {
		log.WithField("error", err).Fatal("Unable to decode configuration into struct")
		os.Exit(3)
	}

	// Create S3 client
	s3Client, err := CreateObjectStorageClient(&config)
	if err != nil {
		log.WithField("error", err).Fatal("S3 client creation failed")
	}

	// Create router
	router := chi.NewRouter()
	if config.Debug == "true" {
		router.Use(logger.LoggerWithLevel("router", log, logrus.DebugLevel))
	} else {
		router.Use(logger.Logger("router", log))
	}

	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(middleware.Timeout(60 * time.Second))

	routerv1 := router.Route("/v1", func(router chi.Router) {})

	// Health check endpoint
	router.Get("/health", func(writer http.ResponseWriter, router *http.Request) {
		writer.Write([]byte("hello :)"))
	})

	// Map endpoint
	routerv1.Mount("/maps", GameMapRoutes())

	http.ListenAndServe(config.WebServer.Address, router)
}

func GameMapRoutes() chi.Router {
	router := chi.NewRouter()
	gameMapHandler := GameMapHandler{
		storage: GameMapStore{},
	}
	router.Get("/", gameMapHandler.ListGameMaps)
	router.Post("/", gameMapHandler.CreateGameMap)
	router.Get("/{author}.{name}", gameMapHandler.GetGameMap)
	// Redundant?
	// router.Put("/{author}.{name}", gameMapHandler.UpdateGameMap)
	router.Delete("/{author}.{name}", gameMapHandler.DeleteGameMap)
	return router
}
